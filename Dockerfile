FROM node:latest

COPY bootstrap.sh /

RUN chmod 777 bootstrap.sh

CMD '/bootstrap.sh'