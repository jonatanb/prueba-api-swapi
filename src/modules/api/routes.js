import http from "http";
import { ENDPOINT_MOVIES, ENDPOINT_SHORT_MOVIES } from "../constants/routes";
import { PORT } from "../constants/app";
import MoviesController from "../classes/MoviesController";
import axios from "axios";

const movies = new MoviesController(axios);

const server = http.createServer(async (req, res) => {
  if (req.url === ENDPOINT_SHORT_MOVIES) {
    try {
      const data = await movies.getShortMovies();
      res.end(JSON.stringify(data));
    } catch (error) {
      res.end(error.message);
    }
  } else if (req.url === ENDPOINT_MOVIES) {
    try {
      const data = await movies.getMovies();
      res.end(JSON.stringify(data));
    } catch (error) {
      res.end(error.message);
    }
  } else {
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write("<html><body><h1>Invalid Request.</h1></body></html>");
    res.end();
  }
});

server.listen(PORT);
console.log(`Listening on port ${PORT}!`);
