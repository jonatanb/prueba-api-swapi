import { API } from "../constants/app";
import { asyncForEach } from "../helpers/utils";

export default class MoviesController {
  constructor(ClientRequest) {
    this.request = ClientRequest;
  }

  async getShortMovies() {
    const { data } = await this.request.get(`${API}/films`);
    return data;
  }

  async getMovies() {
    const data = await this.getShortMovies();
    let movies = [];
    await asyncForEach(data.results, async (item) => {
      let planets = await this.getPlanets({ planetsURLs: item.planets });
      let characters = await this.getCharacters({
        charactersURLs: item.characters,
      });
      let starships = await this.getStarships({
        starshipsURLs: item.starships,
      });
      movies.push({ name: item.title, planets, characters, starships });
    });
    return movies;
  }

  async getPlanets({ planetsURLs }) {
    if (planetsURLs) {
      let planets = [];
      await asyncForEach(planetsURLs, async (url) => {
        const { data } = await this.request.get(`${url}`);
        const { name, terrain, gravity, diameter, population } = data;
        planets.push({ name, terrain, gravity, diameter, population });
      });
      return planets;
    }
    throw new Error("Planets URLs is missing");
  }

  async getCharacters({ charactersURLs }) {
    if (charactersURLs) {
      let characters = [];
      await asyncForEach(charactersURLs, async (url) => {
        const { data } = await this.request.get(`${url}`);
        const {
          name,
          gender,
          hair_color,
          skin_color,
          eye_color,
          height,
          homeworld,
        } = data;
        characters.push({
          name,
          gender,
          hair_color,
          skin_color,
          eye_color,
          height,
          homeworld,
        });
      });
      return characters;
    }
    throw new Error("Characters URLs is missing");
  }

  async getStarships({ starshipsURLs }) {
    if (starshipsURLs) {
      let starships = [];
      await asyncForEach(starshipsURLs, async (url) => {
        const { data } = await this.request.get(`${url}`);
        const { name, model, manufacturer, passengers } = data;
        starships.push({ name, model, manufacturer, passengers });
      });
      return starships;
    }
    throw new Error("Characters URLs is missing");
  }
}
